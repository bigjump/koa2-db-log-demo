## 生产

```bash
$ npm install --production
```
## 开发

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

# production build
$ npm run build
```
http://127.0.0.1:3000
## msg
ts
koa + mongoose + joi + log4js + swagger

## License

  Nest is [MIT licensed](LICENSE).
