import Koa from 'koa'
import db from './lib/db'
import koaBody from 'koa-body'
import { regLog } from './util/log'
import regRoutes from './controller'
import config from './config'

// 注意执行顺序
// initKoa
const app: Koa = new Koa()
// 链接数据库
db.connect()
// koaBody
app.use(koaBody(config.koaBody))
// 注册路由
regRoutes(app)
// 注册日志
regLog(app)
// 监听端口
app.listen(config.port, () => console.log('This server is running at http://127.0.0.1:' + config.port))
