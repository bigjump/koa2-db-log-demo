import Koa from 'koa'
import apiRouter from './api'
import userRouter from './user'
import kjsRouter from '../util/swagger'

export default function regRoutes(app: Koa):void {
  app.use(apiRouter.getRouter().routes())
  app.use(userRouter.getRouter().routes())
  app.use(kjsRouter.getRouter().allowedMethods())
}