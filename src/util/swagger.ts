import { KJSRouter } from 'koa-joi-swagger-ts'
import * as fs from 'fs'

const CONFIG = {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: '接口文档系统'
  },
  host: '127.0.0.1:3000',
  basePath: '',
  schemes: ['http'],
  paths: {},
  definitions: {}
}

const kjsRouter = new KJSRouter(CONFIG)
kjsRouter.setSwaggerFile('swagger.json')
kjsRouter.loadSwaggerUI('/api')
fs.writeFileSync('./swagger.json', JSON.stringify(kjsRouter.getSwaggerFile()))

export default kjsRouter