import mongoose from 'mongoose'
import config from '../config/config.default'

export default {
  async connect(): Promise<void> {
    await mongoose.connect(config.mongodb.url, config.mongodb.options)
  },
  close(): Promise<void> {
    return mongoose.connection.close()
  }
}