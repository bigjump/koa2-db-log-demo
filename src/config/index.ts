import pordConfig from './config.default'
import devConfig from './config.dev'

export default process.env.NODE_ENV === 'development' ? devConfig : pordConfig