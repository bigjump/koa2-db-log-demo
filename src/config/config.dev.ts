import path from 'path'

const mongodb = { // mongoDB
  url: 'mongodb+srv://root:xn123456@cluster0.ll8xw.azure.mongodb.net/topfullstack',
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  plugins: [],
}

const log4Config = {
  appenders: {
    access: {
      compress: true,
      type: 'file',
      maxLogSize: 1 * 1024 * 1024, // 按大小滚动 字节
      backups:4, 
      filename: path.join('logs/', 'access.log'), //生成文件名
    },
    application: {
      compress: true,
      type: 'file',
      maxLogSize: 1 * 1024 * 1024,
      backups:4,
      filename: path.join('logs/', 'application.log'),
    },
    out: {
      type: 'console'
    }
  },
  categories: {
    default: { appenders: ['out'], level: 'ALL' },
    access: { appenders: ['access'], level: 'ALL' },
    application: { appenders: ['application'], level: 'WARN' }
  }
}

const koaBody = { // koaBody配置
  multipart: true,
  // formLimit: 15,
  formidable: {
    keepExtensions: true,
    maxFieldsSize: 2 * 1024 * 1024, // 文件上传大小
    uploadDir: path.join('uploads')
  }
}

export default {
  port: 3000,
  mongodb,
  log4Config,
  koaBody
}