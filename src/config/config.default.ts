import path from 'path'

const mongodb = { // mongoDB
  url: 'mongodb+srv://root:pwd@cluster0.ll8xw.azure.mongodb.net/topfullstack',
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  plugins: [],
}

const log4Config = {
  appenders: {
    access: { // 按日期滚动
      type: 'dateFile', // maxLogSize与dateFile互斥
      pattern: '-yyyy-MM-dd.log', //生成文件的规则
      filename: path.join('logs/', 'access.log'), //生成文件名
      alwaysIncludePattern: true,
    },
    application: {
      type: 'dateFile',
      pattern: '-yyyy-MM-dd.log',
      filename: path.join('logs/', 'application.log'),
      alwaysIncludePattern: true,
    },
    out: {
      type: 'console'
    }
  },
  categories: {
    default: { appenders: ['out'], level: 'info' },
    access: { appenders: ['access'], level: 'info' },
    application: { appenders: ['application'], level: 'WARN' }
  }
}

const koaBody = { // koaBody配置
  multipart: true,
  // formLimit: 15,
  formidable: {
    keepExtensions: true,
    maxFieldsSize: 2 * 1024 * 1024, // 文件上传大小
    uploadDir: path.join('uploads')
  }
}

export default {
  port: 3000,
  mongodb,
  log4Config,
  koaBody
}