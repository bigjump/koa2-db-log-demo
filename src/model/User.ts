import { prop, getModelForClass, ReturnModelType } from '@typegoose/typegoose'

// db
export class User {
  @prop()
  public nickname?: string

  @prop()
  public username?: string
}

export const UserModel: ReturnModelType<typeof User> = getModelForClass(User)