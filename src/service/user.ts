import { UserModel, User } from '../model/User'
import { DocumentType } from '@typegoose/typegoose'

export default {
  async getUserById(userId: string): Promise<DocumentType<User> | null> {
    return await UserModel.findById(userId).exec()
  }
}